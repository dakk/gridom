from pytezos.crypto import Key
from .. import constants

class Account:
    def __init__(self, key):
        self._key = key
        self._address = key.public_key_hash()
        self._public_key = key.public_key()

    def random():
        pass

    @property
    def address(self): 
        return self._address

    def fromPrivateKey(pkey):
        return Account(Key(pkey))

    def sign(self, operation):
        return self._key.sign(operation)

    def faucet():
        return Account.fromPrivateKey(constants.FAUCET['key'])