import logging
import os
import sys
import getopt
import json
from colorlog import ColoredFormatter
from . import constants, State


formatter = ColoredFormatter(
	'%(log_color)s[%(asctime)-8s] %(module)s: %(message_log_color)s%(message)s',
	datefmt=None,
	reset=True,
	log_colors = {
		'DEBUG': 'blue',
		'PLUGINFO': 'purple',
		'INFO':	'green',
		'WARNING': 'yellow',
		'ERROR': 'red',
		'CRITICAL': 'red',
	},
	secondary_log_colors={
		'message': {
			'DEBUG': 'purple',
			'PLUGINFO': 'blue',
			'INFO':	'yellow',
			'WARNING': 'green',
			'ERROR': 'yellow',
			'CRITICAL': 'red',
		}
	},
	style = '%'
)

stream = logging.StreamHandler ()
stream.setFormatter (formatter)

logger = logging.getLogger ('gridom')
logger.addHandler (stream)
logger.setLevel (10)

def usage ():
	print ('usage: gridom [options] action\n')
	print ('Options:')
	print ('  -h, --help\t\t\tdisplay this help')
	print ('  -V, --version\t\t\tdisplay the software version')
	print ('  -c, --config=path\t\tspecify a configuration file')
	print ('  -s, --state=path\t\tpersist simulation state on this file (default: ~/state.gr')
	print ('  -v, --verbose\t\t\tset verbosity level (0-5)')
	print ('  -n, --network=net\t\tspecify the network (alpha, zero, main)')
	print ('')
	print ('Actions:')
	print ('  compile\t\t\tcompile the contract')
	print ('  deploy\t\t\tdeploys the contract into the blockchain')
	print ('  call what [args]\t\tperform a manual call on contract')
	print ('  serve\t\t\t\texecute the oracle on deployed contract')


def start():
	config = None
	stateFile = None
	configFile = None

	try:
		opts, args = getopt.getopt(sys.argv[1:], "Vc:v:hs:", ["state=", "version", "config=", "verbose=", "help"])
	except getopt.GetoptError as e:
		print (e)
		usage()
		sys.exit ()

	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage ()
			sys.exit ()

		elif opt in ("-V", "--version"):
			print (constants.VERSION)
			sys.exit ()

		elif opt in ("-c", "--config"):
			configFile = arg

		elif opt in ("-s", "--state"):
			stateFile = arg

		elif opt in ("-v", "--verbose"):	
			logger.setLevel (60 - int(arg) * 10)

	action = sys.argv[-1]

	logger.info ('gridom is starting')

	if not (action in ['deploy', 'serve', 'call', 'compile']):
		usage()
		sys.exit(0)

	if not configFile and not stateFile:
		logger.critical('No configuration or state file supplied, exiting.')
		sys.exit(0)

	state = None
	config = None


	if not stateFile:
		logger.debug ('Loading config file %s' % configFile)
		with open(configFile, 'r') as fd:
			config = json.loads(fd.read())

		state = State(config)
	else:
		state = State(config)
		state.load(stateFile)

	try:
		state.node().waitSync()
	except:
		logger.critical('Tezos node is not reachable, exiting.')
		sys.exit(0)
	
	if action == 'call':
		pass



if __name__ == "__main__":
	start()