import math
import random
from . import MeterRepository

class MeterRepositorySimulator (MeterRepository):
    def __init__(self, actors):
        self.actors = actors

    def getValues(self):
        fvalues = []

        for a in actors:
            fvalues.push([a['name'], math.floor (random.random() * 2 - 1)])

        return fvalues