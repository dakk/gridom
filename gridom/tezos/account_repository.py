from . import Account

class AccountRepository:
    def __init__(self):
        self._accounts = {}

    """ Create an AccountRepository from a dict {name: pkey, ...} """
    def fromDict(d):
        ar = AccountRepository()
        for k in d:
            ar.add(k, d[k])
        return ar

    def toDict(self):
        return {}

    def get(self, k):
        return self._accounts[k]

    def signWith(self, k, data):
        return self.get(k).sign(data)

    def addressOf(self, k):
        return self.get(k).address()

    def add(self, k, key):
        self._accounts[k] = Account.fromPrivateKey(key)
