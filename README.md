# Gridom

## Installation

```
virtualenv -p python3.6 venv
. venv/bin/activate
python setup.py install
```

## Usage

Compile the contract:
```
gridom -c configs/test1.json compile
```

Deploy the contract:
```
gridom -c configs/test1.json -s ~/simulation.gr deploy
```

Run the oracle daemon:
```
gridom -c configs/test1.json -s ~/simulation.gr serve
```
