import json

""" 
    State of a contract execution environment

    It allows to save the current state or load from a file, and it holds the contract informations
    and all accounts keys.

    {
        "contract": "contract_address",
        "accounts": {
            "smartmeter1": "privatekey"
        },
        "config": { }
    }
"""

from .tezos import AccountRepository, Contract, TezosNode

class State:
    def __init__(self, config):
        self._config = config
        self._node = TezosNode()
        self._contract = None
        self._accounts = None

    def contract(self):
        return self._contract

    def node(self):
        return self._node

    def accounts(self):
        return self._accounts

    def config(self):
        return self._config
        
    def save(self, f):
        data = {
            'config': self._config
        }

        if self._contract and self._contract.isDeployed():
            data['contract'] = self._contract.address()
        else:
            data['contract'] = None

        if self._accounts:
            data['accounts'] = self._accounts.toDict()
        else:
            data['accounts'] = {}

        with open(f, 'w') as fd:
            fd.write(json.dumps(data))

    def load(self, f):
        try:
            with open(f, 'r') as fd:
                data = json.loads(fd.read())
                self._config = data['config']
                if data['contract'] != None:
                    self._contract = Contract.fromDeployedContract(data['contract'])
                else:
                    self._contract = None

                self._accounts = AccountRepository.fromDict(data['accounts'])
        except:
            return False