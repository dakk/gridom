# -*- coding: utf-8 -*-
# Copyright (C) 2019 Davide Gessa

from setuptools import find_packages
from setuptools import setup
from gridom import constants

setup(name=constants.NAME,
	version=constants.VERSION,
	description='',
	author=['Davide Gessa'],
	setup_requires='setuptools',
	author_email=['gessadavide@gmail.com'],
	packages=[
		'gridom',
		'gridom.tezos',
		'gridom.engine',
		'gridom.meter'
	],
	entry_points={
		'console_scripts': [
			'gridom=gridom.main:start'
		],
	},
	install_requires=open ('requirements.txt', 'r').read ().split ('\n'),
)
