class Contract:
    def __init__(self, sourceFile):
        self._deployed = False
        self._address = None
        self._sourceFile = sourceFile

    def isDeployed(self):
        return self._deployed

    def address(self):
        return self._address

    def fromDeployedContract(contractAddress):
        c = Contract(None)
        c._address = contractAddress
        c._deployed = True
        return c._address

    def deploy(self, tezosNode, account, params):
        # ./alphanet.sh client originate contract my_demo1 for my_identity transferring 2.01 from my_account running container:demo.liq.tz -init '0'
        pass

    def call(self, account, amount, call, params):
        os.system('tezos-client transfer %.8f from %s to %s -arg \'%s\'' % (amount, account, self._address, params)
        # ./alphanet.sh client transfer 0 from my_account to my_demo1 -arg '(Pair 5 (Pair 8 (Right (Left Unit))))'

    def storage(self):
        pass
        # ./alphanet.sh client get storage for my_demo1