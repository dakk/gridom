from . import Poc1Engine, Poc1SimulateEngine

class EngineRepository:
    _engines = {
        'poc1': Poc1Engine,
        'poc1_simulate': Poc1SimulateEngine
    }

    def get(name):
        return EngineRepository._engines[name]