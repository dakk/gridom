from . import MeterRepository

class MeterRepositoryUDP (MeterRepository):
    def __init__(self, host, port):
        self.host = host
        self.port = port

    def getValues(self):
        return []