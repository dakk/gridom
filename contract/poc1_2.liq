(* 
Flusso:

  1. L'oracolo deploya il contratto, specifica l'address dell'oracolo, il prezzo in tez di 1 millikw e la soglia di withdraw
  2. I meter consumer / producer caricano con deposit il loro balance interno al contratto; in qualsiasi momento possono
  fare un withdraw per prelevare i fondi
  3. L'oracolo puo' aggiornare il prezzo (in tezos) di un millikw, in seguito al cambiamento del prezzo di cambiamento
  tra tez e euro
  4. Periodicamente l'oracolo chiama la update, dove per ogni meter interessato specifica il quantitativo
di millikw prodotti o consumati; viene aggiornato il balance dei meter in questo modo:

                                                                            nuovo_balance = vecchio_balance (+ o -) w * ppwh

Nota:
  - il prezzo del kw e' in millikw per diminuire la complessita' delle operazioni
                           - il prezzo per milli kw (ppwh) e' in tezos: ai fini della decentralizzazione, dare il prezzo in euro, o direttamente 
in tez non ha nessuna valenza, in quanto e' comunque un valore che si ipotizza "trusted" perche' fornito 
  dall'oracolo
                                  - ho separato update e update_ppwh in quanto svolgono due operazioni diverse; dato che il prezzo del mw potrebbe non
variare ad ogni update; in futuro quando gli smart meter saranno essi stessi a fornire il loro consumo / produzione
                             questa separazione sara' in ogni caso obbligata dato che l'oracolo avra' il solo compito di impostare il prezzo
                                                         *)

[%%version 0.9]

type storage = {
  oracle: address;
  meters_balance: (address, tez) map;
  last_update: timestamp;
  ppwh_tez: tez; (* in tezos, updated on tezprice change *)
  ppwh: tez; (* in euro, fixed *)
  withdraw_threshold: tez 
};;

let%init storage oracle ppwh withdraw_threshold = 
  if withdraw_threshold < 1tz then failwith "withdraw_threshold should be greater or equal to 1";
  if ppwh <= 0tz then failwith "ppwh should be greater than 0";    
  {
    oracle = oracle;
    meters_balance = Map [];
    last_update = Current.time ();
    ppwh = ppwh;
    withdraw_threshold = withdraw_threshold;
  }
;;

(** Helper function which assert the caller is the oracle *)
let assert_oracle storage = if Current.sender() <> storage.oracle then failwith "Only oracle allowed";;

(**
* Request a withdraw for a meter
                         * - amount: amount to withdraw, in tez
                                                            * - dest: destination address
                                                                      *)
let%entry withdraw ((amount, dest): (tez * key_hash))  storage = 
  if amount < storage.withdraw_threshold then failwith "Amount below withdraw threshold";
  if Current.balance () < amount then failwith "Contract has not enough balance";
  match Map.find (Current.sender ()) storage.meters_balance with
  | None -> failwith "Meter not found on contract"
  | Some(b) ->
      if b < amount then failwith "Meter has no sufficient balance";
      let bl = storage.meters_balance in
      let nbl = Map.update (Current.sender ()) (Some (b - amount)) bl in
      let storage = storage.meters_balance <- nbl in
      ([Account.transfer ~amount ~dest:dest], storage)
;;


(** 
 * Deposit to a meter balance
*)
let%entry deposit () storage = 
  let nbl = (match Map.find (Current.sender ()) storage.meters_balance with
      | None -> Map.update (Current.sender ()) (Some (Current.amount ())) storage.meters_balance
      | Some(b) -> Map.update (Current.sender ()) (Some (Current.amount () + b)) storage.meters_balance) in
  let storage = storage.meters_balance <- nbl in
  ([], storage)
;;



(** Oracle can update the tezprice *)
let%entry update_tezprice (tezprice: tez) storage = 
  assert_oracle storage;
  ([], storage.ppwh_tez <- storage.ppwh * tezprice)
;;


(** 
* Update: called by the oracle
          *
          * Params: a list of (address_of_meter, value, producer) where
* - producer: if true, the value is added, subtract otherwise 
* - value: w (kw/1000) consumed / produced
           * - address: is the address of the meter
                                          *)
let%entry update (consuml: (address * nat * bool) list) storage =
  assert_oracle storage;
  let storage = List.fold (fun (x, st) ->  
      let kwvalue = x.(1) * st.ppwh_tez in
      let nbl = match Map.find x.(0) st.meters_balance with
        | None -> 
            if x.(2) then 
              Map.update x.(0) (Some (kwvalue)) st.meters_balance
            else
              failwith "Meter can't have negative balance"
        | Some(b) -> 
            let nv = if x.(2) then b + kwvalue else b - kwvalue in
            Map.update x.(0) (Some (nv)) st.meters_balance
      in
      st.meters_balance <- nbl
    ) consuml storage in
  let storage = storage.last_update <- Current.time () in
  ([], storage)
;;
