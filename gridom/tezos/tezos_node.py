import logging
import time
import threading
import dateutil.parser
from datetime import datetime
from pytezos.rpc import mainnet, alphanet
from .. import constants
from .account import Account

logger = logging.getLogger('gridom')

class TezosNode:
    def __init__ (self, network = 'alpha'):
        if network == 'main':
            self._node = mainnet
        else:
            self._node = alphanet
        logger.info('Connected to node on chain %s' % network)
        self._faucet = Account.faucet()
        logger.debug('Faucet set to %s' % self._faucet.address)
            
    def waitSync(self):
        d = dateutil.parser.parse(self._node.head()['header']['timestamp'])

        if d.date() < datetime.today().date():
            logger.info('Node is out of sync, waiting for current head...')

        while d.date() < datetime.today().date():
            self.headPrint()            
            time.sleep(5)
            d = dateutil.parser.parse(self._node.head()['header']['timestamp'])

    def headPrint(self):
        head = self._node.head()
        logger.debug('Head is %s at height %d: %s' % (head['hash'], head['header']['level'], head['header']['timestamp']))
        time.sleep(1)

    """ Deploy a contract into the chain, returns the contract address """
    def deployContract (self, account, code):
        return ''

    """ Create a contract call from account using params and sending amount """
    def callOnContract (self, account, contract, call, params = None, amount = None):
        return ''

    """ Inject a signed operation """
    def injectOperation(self, op):
        return ''

    """ Block the code untile the opid is confirmed in a block """
    def waitForOperation (self, opid):
        return ''

    """ Create a transaction of amount from the faucet to address """
    def faucetTo(self, address, amount):
        fbal = self.balanceOf(self._faucet.address())
        if fbal < amount:
            logger.error ('Faucet has insufficient funds: %d (%d required)' % (fbal, amount))

        # TODO: create the transaction

    """ Returns the balance of a given address """
    def balanceOf(self, address):
        return self._node.context.contracts[address]()['balance'] / 100000000.
